<?php
return[
    'addnew'=>'add new',
    'username'=>'user name ',
    'email'=>'email',
    'created'=>'created at',
    'updated'=>'updated at',
    'actions'=>'actions',
    'addnew'=>'add new',
    'moderators'=>'Moderators',
    'createmoderator'=>'create moderator',
    'name'=>'name',
    'StatusSelect'=>'Status Select',
    'userStatusSelect'=>'user Status Select',
    'password'=>'password',
    'update'=>'update',
    'deletemsg'=>'are you sure? you want to delete this field?',
    'moderatoredit'=>'moderator edit',
    'moderatorstable'=>'moderators table'

];