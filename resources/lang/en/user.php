<?php
return[
    'admins'=>'admins',
    'users'=>'users',
    'addnew'=>'add new',
    'email'=>'email',
    'created'=>'created at',
    'updated'=>'updated at',
    'actions'=>'actions',
    'AdminsTable'=>'Admins Table',
    'createadmin'=>'create admin',
    'createuser'=>'create user',
    'useredit'=>'edit user',
    'name'=>'name',
    'StatusSelect'=>'Status Select',
    'userStatusSelect'=>'User Status select',
    'password'=>'password',
    'update'=>'update',
    'adminedit'=>'admin edit ',
    'deletemsg'=>'Are you sure? You want to delete this?',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'addnew'=>'add new',
    'username'=>'user name ',
    'email'=>'email',
    'created'=>'created at',
    'updated'=>'updated at',
    'actions'=>'actions',
    'addnew'=>'add new',
    'moderators'=>'Moderators',
    'createmoderator'=>'create moderator',
    'name'=>'name',
    'StatusSelect'=>'Status Select',
    'userStatusSelect'=>'user Status Select',
    'password'=>'password',
    'update'=>'update',
    'deletemsg'=>'are you sure? you want to delete this field?',
    'moderatoredit'=>'moderator edit',
    'moderatorstable'=>'moderators table',
];