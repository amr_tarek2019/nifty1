<?php
return[
    'Sliders'=>'Sliders',
    'addnew'=>'add new',
    'slidersstable'=>'sliders table',
    'ArabicTitle'=>'Arabic Title',
    'EnglishTitle'=>'English Title',
    'Image'=>'Image',
    'arabictext'=>'Arabic Text',
    'EnglishText'=>'English Text',
    'image'=>'image',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'slideredit'=>'Edit Slider',
    'createslider'=>'create slider',
    'editslider'=>'edit slider',
    'actions'=>'actions'


];