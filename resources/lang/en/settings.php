<?php
return[
    'phone'=>'phone',
    'email'=>'Email address',
    'website'=>'website',
    'facebook'=>'facebook',
    'whatsapp'=>'whatsapp',
    'instagram'=>'instagram',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'settings'=>'settings'
];