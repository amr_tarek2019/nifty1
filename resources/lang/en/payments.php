<?php
return[
 'Payments'=>'Payments',
 'username'=>'User Name',
 'stationname'=>'Station Name',
 'Date'=>'Date',
 'FuelAmountPaid'=>'Fuel Amount Paid',
 'DonationAmount'=>'Donation Amount',
 'ordernumber'=>'Order Number',
 'Actions'=>'Actions',
 'PaymentInformation'=>'Payment Information',
 'UserInformation'=>'User Information',
 'Emailaddress'=>'Email address',
 'Phone'=>'Phone',
 'StationInformation'=>'Station Information',
 'EnglishName'=>'English Name',
 'ArabicName'=>'Arabic Name',
 'Regioninarabic'=>'Region in arabic',
 'Regioninenglish'=>'Region in english',
 'Descriptioninenglish'=>'Description in english',
 'Descriptioninarabic'=>'Description in arabic',
 'Rate'=>'Rate',
 'QrCode'=>'Qr Code',
 'Mainimage'=>'Main image',
 'Qrimage'=>'Qr image',
 'icon'=>'icon',
 'stationslider'=>'station slider',
 'back'=>'back',
 'paymentstable'=>'payments table',
    'actions'=>'actions'


];