<?php
return[
    'createCharity'=>'Create Charity',
    'charities'=>'charities',
    'addnew'=>'add new',
    'CharitiesTable'=>'Charities Table',
    'actions'=>'actions',
    'arabicname'=>'arabic name',
    'englishname'=>'english name',
    'image'=>'image',
    'submit'=>'submit',
    'cancel'=>'cancel',
    'updateCharity'=>'update Charity',
];