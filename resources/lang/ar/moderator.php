<?php
return[
    'addnew'=>'اضافة جديد',
    'username'=>'اسم المستخدم ',
    'email'=>'البريد الالكتروني',
    'created'=>'تم إنشاؤه في',
    'updated'=>'تم تعديله في',
    'actions'=>'الاجراءات',
    'addnew'=>'اضافة جديد',
    'moderators'=>'المدراء',
    'users'=>'المستخدمين',
    'createmoderator'=>'إنشاء مدير',
    'name'=>'الاسم',
    'StatusSelect'=>'تحديد الحالة',
    'userStatusSelect'=>'تحديد حاله المستخدم',
    'password'=>'كلمة المرور',
    'update'=>'تعديل',
    'deletemsg'=>'هل انت متأكد؟ هل تريد حذف هذا الحقل',
    'moderatoredit'=>'تعديل المدير',
    'moderatorstable'=>'جدول المدراء'

];
