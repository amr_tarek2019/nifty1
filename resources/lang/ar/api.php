<?php
return[
    'submitted'=>'هذا الحساب مفعل مسبقا',
    'logged'=>'تم تسجيل الدخول',
    'check'=>'تأكد من صحة البيانات',
    'code'=>'تم ارسال الكود بنجاح',
    'verify'=>'تم ادخال الكود بنجاح',
    'codenotfound'=>'كود التفعيل ليس صحيح',
    'passwordchangedsuccess'=>'تم تغيير الرقم السري بنجاح',
    'somethingwentwrong'=>'حدث خطأ ما',
    'getdataprofile'=>'تم ايجاد بيانات المستخدم بنجاح',
    'profiledatachanged'=>'تم تغيير بيانات المستخدم بنجاح',
    'suggestion'=>'تم ارسال المقترح بنجاح',
];
