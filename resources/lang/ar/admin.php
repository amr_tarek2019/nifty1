<?php
return[
    'admins'=>'المشرفين',
    'users'=>'المستخدمين',
    'addnew'=>'اضافة جديد',
    'AdminsTable'=>'جدول المشرفين',
    'email'=>'البريد الالكتروني',
    'created'=>'تم إنشاؤه في',
    'updated'=>'تم تعديله في',
    'actions'=>'الاجراءات',
    'createadmin'=>'إنشاء مشرف',
    'name'=>'الاسم',
    'StatusSelect'=>'تحديد الحالة',
    'userStatusSelect'=>'تحديد حاله المستخدم',
    'password'=>'كلمة المرور',
    'update'=>'تعديل',
    'adminedit'=>'تعديل المشرف',
    'deletemsg'=>'هل انت متأكد؟ هل تريد حذف هذا الحقل',
    'submit'=>'اضف',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'cancel'=>'الغاء',
    'print'=>'طباعة',
    'details'=>'تفاصيل'

];