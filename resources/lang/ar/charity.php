<?php
  return[
'createCharity'=>'انشاء جمعية خيرية',
'charities'=>'الجمعيات الخيرية',
'addnew'=>'اضافة جديد',
'CharitiesTable'=>'جدول الجمعيات الخيرية',
'actions'=>'الاجراءات',
'arabicname'=>'الاسم باللغة العربية',
'englishname'=>'الاسم باللغة الانجليزية',
'image'=>'الصورة',
'submit'=>'اضف',
'cancel'=>'الغاء',
'updateCharity'=>'تعديل جمعية خيرية',
  ];