<?php
return[
    'Sliders'=>'الصور',
    'addnew'=>'اضف جديد',
    'slidersstable'=>'جدول الصور',
    'ArabicTitle'=>'العنوان باللغة العربية',
    'EnglishTitle'=>'العنوان باللغة الانجليزية',
    'Image'=>'صورة',
    'arabictext'=>'النص باللغة العربية',
    'EnglishText'=>'النص باللغة الانجليزية',
    'image'=>'صورة',
    'submit'=>'رفع',
    'cancel'=>'الغاء',
    'slideredit'=>'تعديل صورة',
    'createslider'=>'انشاء سليدر',
    'editslider'=>'تعديل سليدر',
    'actions'=>'اجراءات'


];