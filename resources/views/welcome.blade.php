<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->


<head>
    <meta charset="utf-8" />
    <title>Nifty/</title>
    <link type="text/css" media="all" href="{{ asset('assets/coming/assets/fonts/font-awesome-4.2.0/css/font-awesome.min.css') }}" rel="stylesheet" />

    <!-- Libs CSS -->
    <link type="text/css" media="all" href="{{ asset('assets/coming/assets/boostrap-files/css/bootstrap.min.css" rel="stylesheet') }}" />
    <!-- Animations -->
    <link type="text/css" media="all" href="{{ asset('assets/coming/assets/css/animate.css') }}" rel="stylesheet" />
    <!-- Template CSS -->
    <link type="text/css" media="all" href="{{ asset('assets/coming/assets/css/style.css') }}" rel="stylesheet" />
    <!-- Responsive CSS -->
    <link type="text/css" media="all" href="{{ asset('assets/coming/assets/css/respons.css') }}" rel="stylesheet" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/coming/assets/img/favicons/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/coming/assets/img/favicons/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/coming/assets/img/favicons/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('assets/coming/assets/img/favicons/apple-touch-icon.png') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/dashboard/images/group_6.png') }}" />
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,800italic,800,700italic,700,600italic,600,400italic,300' rel='stylesheet' type='text/css' />

</head>
<body>

<!-- Load page -->
<div class="animationload">
    <div class="loader"></div>
</div>
<!-- End load page -->


<!-- Content Wrapper -->
<div id="wrapper">


    <!-- Carousel -->
    <section>
        <div class="container">
            <img src="{{asset('assets/dashboard/images/Nift.jpg')}}"
                 width="100%">
        </div>
        <!-- end container -->
    </section>
    <!-- end Carousel -->

</div>
<!-- end Content Wrapper -->

<!-- Footer -->
<footer id="footer">
    <div class="container">

        <!-- footer socials -->
        <div class="row">

            <div class="footer_socials col-sm-12 text-center">

                <div class="contact_icons">

                </div>

                <div class=“site-copyright”>
                    <p align="center"> <a href="http://2grand.net/" target=“_blank” class=“grand”><img src="{{ asset('assets/login/img/grandandroid.png') }}" width="100px"></a> جميع الحقوق محفوظة لشركة جراند ©</p>
                </div>
            </div>

        </div>
        <!-- end footer socials -->

    </div>
    <!-- end container -->
</footer>
<!-- end footer -->


<!-- Scripts -->
<script src="{{ asset('assets/coming/assets/js/jquery-1.11.2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/boostrap-files/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.easing.1.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.stellar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.parallax-1.1.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.appear.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/placeholders.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.nicescroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/jquery.lwtCountdown-1.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/coming/assets/js/scripts.js') }}" type="text/javascript"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</body>

</html>
