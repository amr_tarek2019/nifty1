@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('station.edit')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item">{{trans('dashboard.stations')}}</li>
                                <li class="breadcrumb-item active">{{trans('station.edit')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('station.edit')}}</h5>
                        </div>
                        <div class="card-body">
                            <form class="form-wizard" id="regForm" method="POST" action="{{ route('station.update',$station->id) }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                                <div class="tab">
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="first_name"><h4> {{trans('station.ArabicName')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->name_ar}}" name="name_ar" id="name_ar" placeholder="Arabic name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="last_name"><h4>{{trans('station.EnglishName')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->name_en}}" name="name_en" id="name_en" placeholder="English name" required>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="phone"><h4>{{trans('station.ArabicRegionName')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->region_ar}}" name="region_ar" id="region_ar" placeholder="enter region in arabic" required>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="phone"><h4>{{trans('station.EnglishRegionName')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->region_en}}" name="region_en" id="region_en" placeholder="enter region in english" required>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="email"><h4>{{trans('station.rate')}}</h4></label>
                                            <input type="number" class="form-control" value="{{$station->rate}}" name="rate" id="rate" placeholder="enter station rate" required>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="email"><h4>{{trans('station.QrCode')}}</h4></label>
                                            <input type="number" class="form-control" value="{{$station->qr_code}}" name="qr_code" id="qr_code" placeholder="enter qr code" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="first_name"><h4>{{trans('station.Descriptioninarabic')}}</h4></label>
                                            <textarea type="text" class="form-control" name="description_ar" id="description_ar" placeholder="enter description in arabic" required>
                                                {{$station->description_ar}}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="first_name"><h4>{{trans('station.Descriptioninenglish')}}</h4></label>
                                            <textarea type="text" class="form-control"name="description_en" id="description_en" placeholder="enter description in english" required>
                              {{$station->description_en}}
                                </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="first_name"><h4>{{trans('station.Latitude')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->latitude}}" name="latitude" id="latitude" placeholder="Latitude" required>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-xs-6">
                                            <label for="first_name"><h4>{{trans('station.Longitude')}}</h4></label>
                                            <input type="text" class="form-control" value="{{$station->longitude}}" name="longitude" id="longitude" placeholder="Longitude" >
                                        </div>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <img src="{{ asset($station->image) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('station.MainImage')}}</label>
                                            <input type="file" id="image" name="image">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-group">
                                            <img src="{{ asset($station->icon) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('station.Icon')}}</label>
                                            <input type="file" id="icon" name="icon">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-group">
                                            <img src="{{ asset($station->image1) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('station.FirstSliderImage')}}</label>
                                            <input type="file" id="image1" name="image1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group">
                                            <img src="{{ asset($station->image2) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{trans('station.SecondSliderImage')}}</label>
                                            <input type="file" id="image2" name="image2">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="text-right btn-mb">
                                        <button class="btn btn-secondary" id="prevBtn" type="button" onclick="nextPrev(-1)">Previous</button>
                                        <button class="btn btn-primary" id="nextBtn" type="button" onclick="nextPrev(1)">Next</button>
                                    </div>
                                </div>
                                <!-- Circles which indicates the steps of the form:-->
                                <div class="text-center"><span class="step"></span><span class="step"></span><span class="step"></span></div>
                                <!-- Circles which indicates the steps of the form:-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>




































    {{--    <div class="content">--}}
    {{--        <div class="container">--}}

    {{--            <div class="row">--}}
    {{--                <div class="col-xs-12">--}}
    {{--                    @include('dashboard.partials.msg')--}}
    {{--                    <div class="page-title-box">--}}
    {{--                        <h4 class="page-title">Create Station </h4>--}}
    {{--                        <ol class="breadcrumb p-0 m-0">--}}
    {{--                            <li>--}}
    {{--                                <a href="#">Nift</a>--}}
    {{--                            </li>--}}
    {{--                            <li>--}}
    {{--                                <a href="#">Stations </a>--}}
    {{--                            </li>--}}
    {{--                            <li class="active">--}}
    {{--                                Create Station--}}
    {{--                            </li>--}}
    {{--                        </ol>--}}
    {{--                        <div class="clearfix"></div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- end row -->--}}
    {{--            <div class="col-sm-9">--}}
    {{--                <ul class="nav nav-tabs">--}}
    {{--                    <li class="active"><a data-toggle="tab" href="#home">Basic Informations</a></li>--}}
    {{--                    <li><a data-toggle="tab" href="#messages">Descriptions & Location</a></li>--}}
    {{--                    <li><a data-toggle="tab" href="#settings">Pictures</a></li>--}}
    {{--                </ul>--}}

    {{--                <form class="form" method="POST" action="{{ route('station.store') }}" enctype="multipart/form-data">--}}
    {{--                    @csrf--}}
    {{--                <div class="tab-content">--}}
    {{--                    <div class="tab-pane active" id="home">--}}
    {{--                        <hr>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="first_name"><h4>Arabic name</h4></label>--}}
    {{--                                    <input type="text" class="form-control" name="name_a" id="name_a" placeholder="Arabic name" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="last_name"><h4>English name</h4></label>--}}
    {{--                                    <input type="text" class="form-control" name="name_e" id="name_e" placeholder="English name" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="phone"><h4>Region In Arabic</h4></label>--}}
    {{--                                    <input type="text" class="form-control" name="region_a" id="region_a" placeholder="enter region in arabic" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="phone"><h4>Region In English</h4></label>--}}
    {{--                                    <input type="text" class="form-control" name="region_e" id="region_e" placeholder="enter region in english" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="email"><h4>Rate</h4></label>--}}
    {{--                                    <input type="number" class="form-control" name="rate" id="rate" placeholder="enter station rate" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="email"><h4>Qr Code</h4></label>--}}
    {{--                                    <input type="number" class="form-control" name="qr_code" id="qr_code" placeholder="enter qr code" required>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        <hr>--}}

    {{--                    </div><!--/tab-pane-->--}}
    {{--                    <div class="tab-pane" id="messages">--}}

    {{--                        <h2></h2>--}}

    {{--                        <hr>--}}

    {{--                            <div class="form-group">--}}

    {{--                                <div class="col-xs-6">--}}
    {{--                                    <label for="first_name"><h4>Description In Arabic</h4></label>--}}
    {{--                                    <textarea type="text" class="form-control" name="description_a" id="description_a" placeholder="enter description in arabic" required></textarea>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        <div class="form-group">--}}

    {{--                            <div class="col-xs-6">--}}
    {{--                                <label for="first_name"><h4>Description In English</h4></label>--}}
    {{--                                <textarea type="text" class="form-control" name="description_e" id="description_e" placeholder="enter description in english" required>--}}
    {{--                                </textarea>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        <div class="form-group">--}}

    {{--                            <div class="col-xs-6">--}}
    {{--                                <label for="first_name"><h4>Latitude</h4></label>--}}
    {{--                                <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Latitude" required>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="form-group">--}}

    {{--                            <div class="col-xs-6">--}}
    {{--                                <label for="first_name"><h4>Longitude</h4></label>--}}
    {{--                                <input type="text" class="form-control" name="longitude" id="longitude" placeholder="Longitude" >--}}
    {{--                            </div>--}}
    {{--                        </div>--}}


    {{--                    </div><!--/tab-pane-->--}}
    {{--                    <div class="tab-pane" id="settings">--}}


    {{--                        <hr>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="form-group">--}}
    {{--                                    <label class="control-label">Main Image</label>--}}
    {{--                                    <input type="file" class="filestyle" data-buttonname="btn-default" id="image" name="image" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="form-group">--}}
    {{--                                    <label class="control-label">Qr Image</label>--}}
    {{--                                    <input type="file" class="filestyle" data-buttonname="btn-default" id="qr" name="qr" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label class="control-label">Icon</label>--}}
    {{--                                    <input type="file" class="filestyle" data-buttonname="btn-default" id="icon" name="icon" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label class="control-label">First Slider Image</label>--}}
    {{--                                    <input type="file" class="filestyle" data-buttonname="btn-default" id="image1" name="image1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group">--}}

    {{--                                <div class="form-group">--}}
    {{--                                    <label class="control-label">Second Slider Image</label>--}}
    {{--                                    <input type="file" class="filestyle" data-buttonname="btn-default" id="image2" name="image2" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <div class="form-group">--}}
    {{--                                <div class="col-xs-12">--}}
    {{--                                    <br>--}}
    {{--                                    <button class="btn btn-lg btn-success pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>--}}
    {{--                                    <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                    </div>--}}

    {{--                </div><!--/tab-pane-->--}}
    {{--                </form>--}}
    {{--            </div><!--/tab-content-->--}}

    {{--        </div> <!-- container -->--}}



    {{--    </div>--}}


@endsection