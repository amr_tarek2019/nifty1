@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('station.stations')}} </h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item"> {{trans('station.stations')}}</li>

                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <a href="{{ route('station.create') }}" class="btn btn-primary">{{trans('station.addnew')}}</a>
            @include('dashboard.partials.msg')
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('station.StationsTable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('station.ArabicName')}}</th>
                                        <th>{{trans('station.EnglishName')}}</th>
                                        <th>{{trans('station.ArabicRegionName')}}</th>
                                        <th>{{trans('station.EnglishRegionName')}}</th>
                                        <th>{{trans('station.rate')}}</th>
                                        <th>{{trans('station.Icon')}}</th>
                                        <th>{{trans('station.QrCode')}}</th>
                                        <th>{{trans('station.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($stations as $key=>$station)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $station->name_ar }}</td>
                                        <td>{{ $station->name_en }}</td>
                                        <td>{{ $station->region_ar }}</td>
                                        <td>{{ $station->region_en }}</td>
                                        <td>{{ $station->rate }}</td>
                                        <td><img class="img-responsive img-thumbnail" src="{{ asset($station->icon) }}" style="height: 60px; width: 100px" alt=""></td>
                                        <td>  <img src="{{ asset('uploads/station/qr_code/'.  $station->qr_code . '.png') }}" alt="image" class="img-responsive img-thumbnail" width="100"></td>
                                        @if(request()->segment(1)=='en')
                                        <td style="width: 1020px;" >
                                            @else
                                            <td style="width: 1020px;padding: 0px;" >
                                                @endif
                                            <a href="{{ route('station.edit',$station->id) }}" class="btn btn-info">{{trans('admin.edit')}}</a>

                                            <form id="delete-form-{{ $station->id }}" action="{{ route('station.destroy',$station->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-danger " onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $station->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }">{{trans('admin.delete')}}</button>

                                            <a href="{{route('station.qr.print',$station->id)}}" class="btn btn-warning"> {{trans('admin.print')}} </a>

                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


























{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Stations </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Stations--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <a href="{{ route('station.create') }}" class="btn btn-primary">Add New</a>--}}
{{--            @include('dashboard.partials.msg')--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box table-responsive">--}}
{{--                        <h4 class="m-t-0 header-title"><b>Stations Table</b></h4>--}}


{{--                        <table id="datatable" class="table table-striped table-bordered">--}}
{{--                            <thead>--}}


{{--                            <tr>--}}
{{--                                <th>#</th>--}}
{{--                                <th>Arabic Name</th>--}}
{{--                                <th>English Name</th>--}}
{{--                                <th>Arabic Region Name</th>--}}
{{--                                <th>English Region Name</th>--}}
{{--                                <th>Rate</th>--}}
{{--                                <th>Icon</th>--}}
{{--                                <th>Actions</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}


{{--                            <tbody>--}}
{{--                            @foreach($stations as $key=>$station)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $key + 1 }}</td>--}}
{{--                                    <td>{{ $station->name_a }}</td>--}}
{{--                                    <td>{{ $station->name_e }}</td>--}}
{{--                                    <td>{{ $station->region_a }}</td>--}}
{{--                                    <td>{{ $station->region_e }}</td>--}}
{{--                                    <td>{{ $station->rate }}</td>--}}
{{--                                    <td><img class="img-responsive img-thumbnail" src="{{ asset($station->icon) }}" style="height: 100px; width: 100px" alt=""></td>--}}
{{--                                    <td>--}}
{{--                                        <a href="{{ route('station.edit',$station->id) }}" class="btn btn-info btn-sm"><i class="material-icons"><i class="mdi mdi-pencil-box"></i></i></a>--}}

{{--                                        <form id="delete-form-{{ $station->id }}" action="{{ route('station.destroy',$station->id) }}" style="display: none;" method="POST">--}}
{{--                                            @csrf--}}
{{--                                        </form>--}}
{{--                                        <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){--}}
{{--                                                event.preventDefault();--}}
{{--                                                document.getElementById('delete-form-{{ $station->id }}').submit();--}}
{{--                                                }else {--}}
{{--                                                event.preventDefault();--}}
{{--                                                }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}

{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}



{{--        </div> <!-- container -->--}}
{{--    </div>--}}

@endsection


