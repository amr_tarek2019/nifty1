<!DOCTYPE html>
<html lang="ar" dir=”rtl”>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" dir="rtl"><head>
    <head>
        <style>
            @import url('https://fonts.googleapis.com.css?family=Open+Sans:400,700');
            *{
                margin: 0;
                padding: 0;
                line-height: 1.5;
                font-family: 'XB Riyaz';
                color: #1b1e21;
                direction: rtl;
            }

        </style>
    </head>
<body>

<div class="content">
    <div class="container">



        <!-- end row -->

        <div class="property-detail-wrapper">


                <div class="col-md-4">
                    <div class="text-center card-box">
                        <div class="text-left">
                            <h4 class="header-title m-t-0 m-b-20">Station</h4>
                        </div>
                        <div class="member-card">
                            <div class="thumb-xl member-thumb m-b-10 center-block">
                                <img src="{{ asset($station->icon) }}" class="img-circle img-thumbnail" alt="profile-image" style="height: 100px; width: 100px">
                                <i class="mdi mdi-star-circle member-star text-success" title="Featured Agent"></i>
                            </div>

                            <div class="">
                                <h4 class="m-b-5">{{ $station->name_e }}</h4>
                                <p>
                                    <span>rate : {{ $station->rate }}</span>
                                </p>
                            </div>
                        </div> <!-- end membar card -->
                    </div> <!-- end card-box -->


                    <!-- end card-box -->
                </div>
            <div class="col-md-1">
                <h4>Qr Code</h4>
                <div id="bx-pager" class="text-center hide-phone">
                    <img src="{{ asset('uploads/station/qr_code/'.  $station->qr_code . '.png') }}" alt="slide-image" height="300" width="300" style="margin-left: 200px">
                </div>
                <!-- end slider -->

            </div>
        </div> <!-- end col -->




        <!-- end property-detail-wrapper -->



    </div> <!-- container -->
    </div>
</div>
</body>
</html>