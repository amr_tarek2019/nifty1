@extends('dashboard.layouts.master')
@section('content')
{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Privacy</h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">Nift</a>--}}
{{--                            </li>--}}

{{--                            <li class="active">--}}
{{--                                Privacy--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    @include('dashboard.partials.msg')--}}
{{--                    <div class="card-box">--}}



{{--                        <div class="row m-t-50">--}}
{{--                            <div class="col-sm-6 col-xs-12 m-t-20">--}}
{{--                                <div class="p-20" style="--}}
{{--    margin-top: -70px;--}}
{{--">--}}
{{--                                    <form class="form-horizontal " action="{{route('privacy.update')}}" method="POST" >--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Aabic Text</label>--}}
{{--                                            <div>--}}
{{--                                                <textarea required class="form-control" name="text_a" id="text_a"  cols="8" rows="8">{{$privacy->text_a}}</textarea>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="form-group">--}}
{{--                                            <label>English Text</label>--}}
{{--                                            <div>--}}
{{--                                                <textarea required class="form-control" name="text_e" id="text_e"  cols="8" rows="8">{{$privacy->text_e}}</textarea>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div>--}}
{{--                                                <button type="submit" class="btn btn-primary waves-effect waves-light">--}}
{{--                                                    Submit--}}
{{--                                                </button>--}}
{{--                                                <button type="reset" class="btn btn-default waves-effect m-l-5">--}}
{{--                                                    Cancel--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </form>--}}
{{--                                </div>--}}

{{--                            </div>--}}


{{--                        </div>--}}
{{--                        <!-- end row -->--}}


{{--                    </div> <!-- end ard-box -->--}}
{{--                </div><!-- end col-->--}}

{{--            </div>--}}
{{--            <!-- end row -->--}}


{{--        </div> <!-- container -->--}}


{{--</div>--}}















<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('privacy.Privacy')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                            <li class="breadcrumb-item active">{{trans('privacy.Privacy')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('privacy.Privacy')}}</h5>
                    </div>
                    <form class="form theme-form" action="{{route('privacy.update')}}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('privacy.arabictext')}}</label>
                                        <div class="col-sm-9">
                                            <textarea required class="form-control" name="text_ar" id="text_ar"  cols="8" rows="8">{{$privacy->text_ar}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('privacy.EnglishText')}}</label>
                                        <div class="col-sm-9">
                                            <textarea required class="form-control" name="text_en" id="text_en"  cols="8" rows="8">{{$privacy->text_en}}</textarea>
                                        </div>
                                    </div>
                              
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-sm-9 offset-sm-3">
                                <button class="btn btn-primary" type="submit">{{trans('privacy.submit')}}</button>
                                <input class="btn btn-light" type="reset" value="Cancel">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>

@endsection