<!DOCTYPE html>
<html lang="ar" dir=”rtl”>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" dir="rtl"><head>
    <head>
        <style>
            @import url('https://fonts.googleapis.com.css?family=Open+Sans:400,700');
            *{
                margin: 0;
                padding: 0;
                line-height: 1.5;
                font-family: 'XB Riyaz';
                color: #1b1e21;
                direction: rtl;
            }

        </style>
    </head>
<body>

<div class="container">





    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">
                    <h4>Invoice</h4>
                </div> -->
                <div class="panel-body">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3><img src="{{asset('assets\dashboard\images\group_6.png')}}" alt="" height="24"> Nift</h3>
                        </div>
                        <div class="pull-right">
                            <h4>Invoice # <br>
                                <strong>{{$data->order_number}}</strong>
                            </h4>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="pull-left m-t-30">
                                <address>
                                    <strong>{{$data->user->name}}</strong><br>
                                    {{$data->user->email}}<br>
                                    <abbr title="Phone">P:</abbr> {{$data->user->phone}}
                                </address>
                            </div>
                            <div class="pull-right m-t-30">
                                <p><strong>Order Date: </strong> {{$data->date}}</p>
                                <p><strong>Order ID: </strong>{{$data->order_number}}</p>
                            </div>
                        </div><!-- end col -->
                    </div>
                    <!-- end row -->

                    <div class="m-h-50"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table m-t-30">
                                    <thead>
                                    <tr><th>#</th>
                                        <th>User Name</th>
                                        <th>Station Name</th>
                                        <th>date</th>
                                        <th>Fuel Amount Paid</th>
                                        <th>Donation Amount</th>
                                    </tr></thead>
                                    <tbody>

                                        <tr role="row" class="odd">
                                            <td class="sorting_1" tabindex="0">{{ $data->id}}</td>
                                            <td>{{ $data->user->name }}</td>
                                            <td>{{ $data->station->name_a }}</td>
                                            <td>{{ $data->date}}</td>
                                            <td>{{ $data->Fuel_Amount_Paid}}</td>
                                            <td>{{ $data->Donation_Amount}}</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="clearfix m-t-40">
                                <h5 class="small text-inverse font-600">PAYMENT TERMS AND POLICIES</h5>

                                <small>
                                 {{$privacy->text_e}}
                                </small>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                            <hr>
                            <h3 class="text-right">Total: {{ $data->sum('Fuel_Amount_Paid','Donation_Amount')}}</h3>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
</body>
</html>