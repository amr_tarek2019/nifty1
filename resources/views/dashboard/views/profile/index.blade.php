@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('profile.AdminProfile')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item active">{{trans('profile.EditAdminProfile')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-8">
                        <form class="card" style="
    width: 1050px;
"  method="POST" action="{{ route('profile.update',Auth::user()->id) }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">{{trans('profile.EditAdminProfile')}}</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('profile.Name')}}</label>
                                            <input class="form-control"  type="text" name="name" id="name" value="{{$user->name}}" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">

                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('profile.email')}}</label>
                                            <input class="form-control" type="text"  name="email" id="email" value="{{$user->email}}" placeholder="Enter email">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <img src="{{ asset($user->image) }}" alt="image" class="img-responsive img-thumbnail" width="100">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <label class="form-label">{{trans('profile.image')}}</label>
                                            <input type="file" id="image" name="image" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">

                                    </div>
                                    <div class="col-md-5">

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">{{trans('profile.password')}}</label>
                                            <input class="form-control" type="password" name="password" id="password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">{{trans('profile.submit')}}</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection