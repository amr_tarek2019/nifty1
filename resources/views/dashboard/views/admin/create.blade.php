@extends('dashboard.layouts.master')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('admin.createadmin')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item">{{trans('admin.admins')}}</li>
                                <li class="breadcrumb-item active">{{trans('admin.createadmin')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('admin.createadmin')}}</h5>
                        </div>
                        <form class="form theme-form" method="POST" action="{{ route('admin.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="username">{{trans('admin.name')}}</label>
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="useremail">{{trans('admin.email')}}</label>
                                            <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email">
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="exampleFormControlSelect9">{{trans('admin.userStatusSelect')}}</label>
                                                <select class="form-control digits" name="user_status" id="user_status">
                                                    <option value="0">Deactive</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="exampleFormControlSelect9">{{trans('admin.StatusSelect')}}</label>
                                                <select class="form-control digits" name="status" id="status">
                                                    <option value="0">Deactive</option>
                                                    <option value="1">Active</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="message">{{trans('admin.password')}}</label>
                                            <input class="form-control" type="password" name="password" id="password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary" type="submit">{{trans('admin.submit')}}</button>
                                    <a href="{{route('admin.index')}}" class="btn btn-light" type="reset" >{{trans('admin.cancel')}}</a>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
















{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Create Admin </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="{{route('dashboard')}}">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="{{route('admin.index')}}">Admins </a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Create Admin--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12 col-md-4">--}}
{{--                                @include('dashboard.partials.msg')--}}

{{--                                <div class="card-box">--}}
{{--                                    <h4 class="header-title m-t-0 m-b-30">Create Admin </h4>--}}

{{--                                    <form method="POST" action="{{ route('admin.store') }}" enctype="multipart/form-data">--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="username">Name</label>--}}
{{--                                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="useremail">Email address</label>--}}
{{--                                            <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email">--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-6 col-md-6">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label class="form-label" for="exampleFormControlSelect9">User Status select</label>--}}
{{--                                                <select class="form-control digits" name="user_status" id="user_status">--}}
{{--                                                    <option value="0">Deactive</option>--}}
{{--                                                    <option value="1">Active</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-sm-6 col-md-6">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label class="form-label" for="exampleFormControlSelect9">Status select</label>--}}
{{--                                                <select class="form-control digits" name="status" id="status">--}}
{{--                                                    <option value="0">Deactive</option>--}}
{{--                                                    <option value="1">Active</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="message">password</label>--}}
{{--                                            <input class="form-control" type="password" name="password" id="password">--}}
{{--                                        </div>--}}
{{--                                        <button type="submit" class="btn btn-purple waves-effect waves-light">Submit</button>--}}
{{--                                    </form>--}}
{{--                                </div> <!-- end card-box -->--}}

{{--                            </div> <!-- end col -->--}}



{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- End row -->--}}



{{--        </div> <!-- container -->--}}

{{--    </div> <!-- content -->--}}

@endsection