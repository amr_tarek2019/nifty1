@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('charity.CharitiesTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item">{{trans('charity.charities')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <a href="{{ route('charity.create') }}" class="btn btn-primary">{{trans('charity.addnew')}}</a>
            @include('dashboard.partials.msg')
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('charity.CharitiesTable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('charity.arabicname')}}</th>
                                        <th>{{trans('charity.englishname')}}</th>
                                        <th>{{trans('charity.image')}}</th>
                                        <th>{{trans('charity.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($charities as $key=>$charity)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $charity->name_ar }}</td>
                                            <td>{{ $charity->name_en }}</td>
                                            <td><img class="img-responsive img-thumbnail" src="{{ asset($charity->image) }}" style="height: 100px; width: 100px" alt=""></td>
                                            <td>
                                                <a href="{{ route('charity.edit',$charity->id) }}" class="btn btn-info active">{{trans('admin.edit')}}</a>

                                                <form id="delete-form-{{ $charity->id }}" action="{{ route('charity.destroy',$charity->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger active" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $charity->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



























{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Charities </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Charities--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <a href="{{ route('charity.create') }}" class="btn btn-primary">Add New</a>--}}
{{--            @include('dashboard.partials.msg')--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box table-responsive">--}}
{{--                        <h4 class="m-t-0 header-title"><b>Charities Table</b></h4>--}}


{{--                        <table id="datatable" class="table table-striped table-bordered">--}}
{{--                            <thead>--}}


{{--                            <tr>--}}
{{--                                <th>#</th>--}}
{{--                                <th>Arabic Name</th>--}}
{{--                                <th>English Name</th>--}}
{{--                                <th>Image</th>--}}
{{--                                <th>Actions</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}


{{--                            <tbody>--}}
{{--                            @foreach($charities as $key=>$charity)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $key + 1 }}</td>--}}
{{--                                    <td>{{ $charity->name_a }}</td>--}}
{{--                                    <td>{{ $charity->name_e }}</td>--}}
{{--                                    <td><img class="img-responsive img-thumbnail" src="{{ asset($charity->image) }}" style="height: 100px; width: 100px" alt=""></td>--}}
{{--                                    <td>--}}
{{--                                        <a href="{{ route('charity.edit',$charity->id) }}" class="btn btn-info btn-sm"><i class="material-icons"><i class="mdi mdi-pencil-box"></i></i></a>--}}

{{--                                        <form id="delete-form-{{ $charity->id }}" action="{{ route('charity.destroy',$charity->id) }}" style="display: none;" method="POST">--}}
{{--                                            @csrf--}}
{{--                                        </form>--}}
{{--                                        <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){--}}
{{--                                                event.preventDefault();--}}
{{--                                                document.getElementById('delete-form-{{ $charity->id }}').submit();--}}
{{--                                                }else {--}}
{{--                                                event.preventDefault();--}}
{{--                                                }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}

{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}



{{--        </div> <!-- container -->--}}
{{--    </div>--}}

@endsection


