@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('charity.createCharity')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item">{{trans('charity.charities')}}</li>
                                <li class="breadcrumb-item active">{{trans('charity.createCharity')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">


                    <div class="card">
                        <div class="card-header">
                            <h5>  {{trans('charity.createCharity')}}</h5>
                        </div>
                        <form class="form theme-form" method="POST" action="{{ route('charity.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('charity.arabicname')}}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name_ar" parsley-trigger="change" required
                                                       placeholder="Enter Arabic Name" class="form-control" id="name_ar">                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('charity.englishname')}}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name_en" parsley-trigger="change" required
                                                       placeholder="Enter English Name" class="form-control" id="name_en">                                          </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('charity.image')}}</label>
                                            <div class="col-sm-9">
                                                <input type="file" id="image" name="image">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        {{trans('charity.submit')}}
                                    </button>
                                    <button type="reset" class="btn btn-default waves-effect m-l-5">
                                        {{trans('charity.cancel')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





















{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Create Charity </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">Charities </a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Create Charity--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="card-box">--}}

{{--                        <div class="row">--}}
{{--                            <div class="col-sm-12 col-xs-12 col-md-6">--}}


{{--                                <div class="p-20">--}}
{{--                                    <form method="POST" action="{{ route('charity.store') }}" enctype="multipart/form-data" data-parsley-validate novalidate>--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="userName">Arabic Name</label>--}}
{{--                                            <input type="text" name="name_a" parsley-trigger="change" required--}}
{{--                                                   placeholder="Enter Arabic Name" class="form-control" id="name_a">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="userName">English Name</label>--}}
{{--                                            <input type="text" name="name_e" parsley-trigger="change" required--}}
{{--                                                   placeholder="Enter English Name" class="form-control" id="name_e">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="control-label">Default file input</label>--}}
{{--                                            <input type="file" class="filestyle" data-buttonname="btn-default" id="image" name="image" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group text-right m-b-0">--}}
{{--                                            <button class="btn btn-primary waves-effect waves-light" type="submit">--}}
{{--                                                Submit--}}
{{--                                            </button>--}}
{{--                                            <button type="reset" class="btn btn-default waves-effect m-l-5">--}}
{{--                                                Cancel--}}
{{--                                            </button>--}}
{{--                                        </div>--}}

{{--                                    </form>--}}
{{--                                </div>--}}

{{--                            </div>--}}


{{--                        </div>--}}


{{--                    </div> <!-- end ard-box -->--}}
{{--                </div><!-- end col-->--}}

{{--            </div>--}}
{{--            <!-- end row -->--}}


{{--        </div> <!-- container -->--}}



{{--    </div>--}}


@endsection