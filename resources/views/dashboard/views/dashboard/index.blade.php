@extends('dashboard.layouts.master')
@section('content')

    <!-- Start content -->
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('dashboard.dashboard')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item"> {{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item active">{{trans('dashboard.dashboard')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 xl-100">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="chart-widget-dashboard">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-0 f-w-600"><span class="counter">{{ \App\User::where('user_type', 'user')->count() }}</span></h5>
                                                <p>{{trans('dashboard.totalUsers')}}</p>
                                            </div><i data-feather="user"></i>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div class="small-chart-gradient-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="chart-widget-dashboard">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-0 f-w-600"><span class="counter">{{ \App\UserStation::all()->count() }}</span></h5>
                                                <p>{{trans('dashboard.totalorders')}}</p>
                                            </div><i data-feather="shopping-cart"></i>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div class="small-chart-gradient-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="chart-widget-dashboard">
                                        <div class="media">
                                            <div class="media-body">
                                                <h5 class="mt-0 mb-0 f-w-600"><span class="counter">{{ \App\Charity::all()->count() }}</span></h5>
                                                <p>{{trans('dashboard.totalcharities')}}</p>
                                            </div><i data-feather="home"></i>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div class="small-chart-gradient-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('dashboard.users')}}</h5>
                                </div>
                                <div class="card-body charts-box">
                                    <div class="flot-chart-container">
                                        <div class="flot-chart-placeholder" id="graph123"></div>
                                    </div>
                                    <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                        <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('dashboard.stations')}}</h5>
                                </div>
                                <div class="card-body charts-box">
                                    <div class="flot-chart-container">
                                        <div class="flot-chart-placeholder" id="mySecondChart"></div>
                                    </div>
                                    <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                        <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('dashboard.charities')}}</h5>
                                </div>
                                <div class="card-body charts-box">
                                    <div class="flot-chart-container">
                                        <div class="flot-chart-placeholder" id="myThirdChart"></div>
                                    </div>
                                    <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                        <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card height-equal" style="min-height: 528px;">
                                <div class="card-header card-header-border">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5>{{trans('dashboard.recentUsers')}}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="new-users">
                                        @php
                                            $users = \App\User::where('user_type','user')->take('4')->orderBy('id','desc')->get();
                                        @endphp
                                        @foreach($users as $user)
                                        <div class="media">
                                            <img src="{{ asset($user->image) }}" alt="user" class="thumb-sm img-circle" />
                                            @if(request()->segment(1)=='en')
                                            <div class="media-body" style="margin-left: 10px">
                                                @else
                                                    <div class="media-body" style="margin-right: 10px">
                                                        @endif
                                                <h6 class="mb-0 f-w-700">{{$user->name}}</h6>
                                                <p>{{$user->email}}</p>
                                            </div><span class="pull-right">
                       <td>{{$user->phone}}</td>
                                   <br> <td>{{$user->created_at}}</td>
                                            </span> </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card height-equal" style="min-height: 528px;">
                                <div class="card-header card-header-border">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5>{{trans('dashboard.recentStations')}}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="new-users">
                                        @php
                                            $stations = \App\Station::take('4')->orderBy('id','desc')->get();                                        @endphp
                                        @foreach($stations as $station)
                                            <div class="media">
                                                <img class="img-responsive img-thumbnail" src="{{ asset($station->icon) }}" style="height: 100px; width: 100px" alt="">
                                                @if(request()->segment(1)=='en')
                                                <div class="media-body" style="margin-left: 10px">
                                                    @else
                                                        <div class="media-body" style="margin-right: 10px">
                                                            @endif
                                                    <h6 class="mb-0 f-w-700">
                                                        @if(request()->segment(1)=='en')
                                                        {{$station->name_en}}
                                                    @else
                                                            {{$station->name_ar}}
                                                            @endif
                                                    </h6>
                                                    <p>
                                                        @if(request()->segment(1)=='en')
                                                        {{$station->region_en}}
                                                    @else
                                                            {{$station->region_ar}}
                                                            @endif
                                                    </p>
                                                </div><span class="pull-right">
                       <td>{{$station->rate}}</td>
                                                    <br> <td>{{$station->qr_code}}</td>
                                   <br> <td>{{$user->created_at}}</td>
                                            </span> </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card height-equal" style="min-height: 528px;">
                                <div class="card-header card-header-border">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5>{{trans('dashboard.recentcharities')}}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="new-users">
                                        @php
                                            $charities = \App\Charity::take('4')->orderBy('id','desc')->get();
                                        @endphp
                                        @foreach($charities as $charity)
                                            <div class="media">
                                                <img src="{{ asset($charity->image) }}" alt="user" class="thumb-sm img-circle" />
                                                @if(request()->segment(1)=='en')
                                                <div class="media-body" style="
    text-align: right;
">
                                                    @else
                                                        <div class="media-body" style="
    text-align: left;
">
                                                            @endif
                                                    @if(request()->segment(1)=='en')
                                                    <h6 class="mb-0 f-w-700">{{$charity->name_en}}</h6>
                                                    @else
                                                        <h6 class="mb-0 f-w-700">{{$charity->name_ar}}</h6>
                                                    @endif
                                                    <p>{{$charity->created_at}}</p>
                                                </div>

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 xl-50">
                            <div class="card">
                                <div class="card-header">
                                    <h5>{{trans('dashboard.recentrequests')}}</h5>

                                </div>
                                <div class="card-body">
                                    <div class="table-responsive shopping-table text-center">
                                        <table class="table table-bordernone">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('dashboard.stationname')}}</th>
                                                <th>{{trans('dashboard.username')}}</th>
                                                <th>{{trans('dashboard.FuelAmountPaid')}}</th>
                                                <th>{{trans('dashboard.donationAmount')}}</th>
                                                <th>{{trans('dashboard.Ordernumber')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $payments = \App\UserStation::take('4')->orderBy('id','desc')->get();
                                            @endphp
                                            @foreach($payments as $key=>$payment)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>
                                                    @if(request()->segment(1)=='en')
                                                    <h5 class="m-0">{{ $payment->station->name_en }}</h5>
                                                        @else
                                                        <h5 class="m-0">{{ $payment->station->name_ar }}</h5>
                                                        @endif
                                                </td>
                                                <td>{{ $payment->user->name }}</td>
                                                <td>{{$payment->Fuel_Amount_Paid}}</td>
                                                <td>{{$payment->Donation_Amount}}</td>
                                                <td>{{$payment->order_number}}</td>
                                            </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head7" title="" data-original-title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                        <pre class=" language-html"><code class=" language-html" id="example-head7"><span class="token comment" spellcheck="true">&lt;!-- Cod Box Copy begin --&gt;</span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table-responsive shopping-table text-center<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>table</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table table-bordernone<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>thead</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>No<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Product<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Quantity<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Status<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Amount<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span> <span class="token attr-name">scope</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Delete<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>thead</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tbody</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Computer<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>5<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-pill<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Active<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>15000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">data-feather</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>x<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Headphone<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>8<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-pill<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Disable<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>8000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">data-feather</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>x<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Furniture<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-pill<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Paused<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>12000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">data-feather</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>x<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>4<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>shoes<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>9<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary btn-pill<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>On Way<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>5500<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">data-feather</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>x<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tbody</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>table</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token comment" spellcheck="true">&lt;!-- Cod Box Copy end --&gt;</span></code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        var morris_chart = {
            init: function() {
                Morris.Area({
                    element: 'graph123',
                    behaveLikeLine: true,
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,0.05)",
                    scaleGridLineWidth: 0.2,
                    data: [
                            @foreach($numbers_of_users as $user)
                        {
                            x: '{{$user->date}}',
                            z: '{{$user->count}}'
                        },
                        @endforeach

                    ],
                    xkey: 'x',
                    ykeys: ['z'],
                    labels: ['Z'],
                    lineColors: [endlessAdminConfig.primary],
                })
            }
        };
        (function($) {
            "use strict";
            morris_chart.init()
        })(jQuery);
    </script>

    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'mySecondChart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_stations as $station)

                { day: '{{$station->date}}', value: '{{$station->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>

    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'myThirdChart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbers_of_charities as $charity)

                { day: '{{$charity->date}}', value: '{{$charity->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>
@stop