@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('settings.settings')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item active">{{trans('settings.settings')}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                    <div class="card">
                        @include('dashboard.partials.msg')
                        <div class="card-header">
                            <h5>{{trans('settings.settings')}}</h5>
                        </div>
                        <form class="form theme-form" action="{{route('settings.update')}}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.phone')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="phone" id="phone"
                                                       value="{{$settings->phone}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.email')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="email" name="email" id="email"
                                                       value="{{$settings->email}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.website')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" id="website" name="website"  value="{{$settings->website}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.facebook')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control digits"name="facebook" type="text" id="facebook"
                                                       value="{{$settings->facebook}}" placeholder="Number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.whatsapp')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control m-input digits" type="text" id="whatsapp"
                                                       value="{{$settings->whatsapp}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('settings.instagram')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="instagram"  id="instagram"
                                                       value="{{$settings->instagram}}">
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        {{trans('settings.submit')}}
                                    </button>
                                    <a href="{{route('dashboard')}}" class="btn btn-default waves-effect m-l-5">
                                        {{trans('settings.cancel')}}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection