@extends('dashboard.layouts.master')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('slider.Sliders')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item active"> {{trans('slider.Sliders')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->

                <div class="col-sm-12">
                    <a href="{{ route('slider.create') }}" class="btn btn-primary">{{trans('slider.addnew')}} </a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('slider.Sliders')}} </h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('slider.ArabicTitle')}}</th>
                                        <th>{{trans('slider.EnglishTitle')}}</th>
                                        <th>{{trans('slider.Image')}}</th>
                                        <th>{{trans('slider.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sliders as $key=>$slider)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $slider->title_ar}}</td>
                                            <td>{{ $slider->title_en }}</td>
                                            <td><img class="img-responsive img-thumbnail" src="{{ asset($slider->image) }}" style="height: 100px; width: 100px" alt=""></td>
                                            <td>
                                                <a href="{{ route('slider.edit',$slider->id) }}" class="btn btn-info ">{{trans('admin.edit')}}</a>

                                                <form id="delete-form-{{ $slider->id }}" action="{{ route('slider.destroy',$slider->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger " onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $slider->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Sliders </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Sliders--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <a href="{{ route('slider.create') }}" class="btn btn-primary">Add New</a>--}}
{{--            @include('dashboard.partials.msg')--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box table-responsive">--}}
{{--                        <h4 class="m-t-0 header-title"><b>Sliders Table</b></h4>--}}


{{--                        <table id="datatable" class="table table-striped table-bordered">--}}
{{--                            <thead>--}}


{{--                                <tr>--}}
{{--                                <th>#</th>--}}
{{--                                <th>Arabic Title</th>--}}
{{--                                <th>English Title</th>--}}
{{--                                <th>Image</th>--}}
{{--                                <th>Actions</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}


{{--                            <tbody>--}}
{{--                            @foreach($sliders as $key=>$slider)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $key + 1 }}</td>--}}
{{--                                <td>{{ $slider->title_a }}</td>--}}
{{--                                <td>{{ $slider->title_e }}</td>--}}
{{--                                <td><img class="img-responsive img-thumbnail" src="{{ asset($slider->image) }}" style="height: 100px; width: 100px" alt=""></td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('slider.edit',$slider->id) }}" class="btn btn-info btn-sm"><i class="material-icons"><i class="mdi mdi-pencil-box"></i></i></a>--}}

{{--                                    <form id="delete-form-{{ $slider->id }}" action="{{ route('slider.destroy',$slider->id) }}" style="display: none;" method="POST">--}}
{{--                                        @csrf--}}
{{--                                    </form>--}}
{{--                                    <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){--}}
{{--                                            event.preventDefault();--}}
{{--                                            document.getElementById('delete-form-{{ $slider->id }}').submit();--}}
{{--                                            }else {--}}
{{--                                            event.preventDefault();--}}
{{--                                            }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            @endforeach--}}

{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}



{{--        </div> <!-- container -->--}}
{{--    </div>--}}






















@endsection


