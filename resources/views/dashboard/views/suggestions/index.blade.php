@extends('dashboard.layouts.master')
@section('content')
{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">Suggestions </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="{{route('dashboard')}}">Nift</a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                Suggestions--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}


{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box table-responsive">--}}
{{--                        <h4 class="m-t-0 header-title"><b>Suggestions Table</b></h4>--}}

{{--                        <table id="datatable" class="table table-striped table-bordered">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th>#</th>--}}
{{--                                <th>Name</th>--}}
{{--                                <th>email</th>--}}
{{--                                <th>suggestion</th>--}}
{{--                                <th>Created At</th>--}}
{{--                                <th>Actions</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}


{{--                            <tbody>--}}
{{--                            @foreach($suggestions as $key=>$suggestion)--}}
{{--                            <tr>--}}
{{--                                <td>{{ $key + 1 }}</td>--}}
{{--                                <td>{{$suggestion->user->name}}</td>--}}
{{--                                <td>{{$suggestion->email}}</td>--}}
{{--                                <td>{{$suggestion->suggestion}}</td>--}}
{{--                                <td>{{$suggestion->created_at}}</td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{ route('suggestion.show',$suggestion->id) }}" class="btn btn-info active"><i class="material-icons"><i class="mdi mdi-eye"></i></i></a>--}}

{{--                                    <form id="delete-form-{{ $suggestion->id }}" action="{{ route('suggestion.destroy',$suggestion->id) }}" style="display: none;" method="POST">--}}
{{--                                        @csrf--}}
{{--                                    </form>--}}
{{--                                    <button type="button" class="btn btn-danger active" onclick="if(confirm('Are you sure? You want to delete this?')){--}}
{{--                                            event.preventDefault();--}}
{{--                                            document.getElementById('delete-form-{{ $suggestion->id }}').submit();--}}
{{--                                            }else {--}}
{{--                                            event.preventDefault();--}}
{{--                                            }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            @endforeach--}}


{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}





{{--        </div> <!-- container -->--}}
{{--    </div>--}}


<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('suggestion.suggestionsTable')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('suggestion.suggestions')}}</li>
                            <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                            <li class="breadcrumb-item active">{{trans('suggestion.suggestions')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5> {{trans('suggestion.suggestionsTable')}} </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('suggestion.Name')}}</th>
                                    <th>{{trans('suggestion.email')}}</th>
                                    <th>{{trans('suggestion.suggestion')}}</th>
                                    <th>{{trans('suggestion.CreatedAt')}}</th>
                                    <th>{{trans('suggestion.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($suggestions as $key=>$suggestion)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{$suggestion->user->name}}</td>
                                        <td>{{$suggestion->email}}</td>
                                        <td>{{$suggestion->suggestion}}</td>
                                        <td>{{$suggestion->created_at}}</td>
                                        <td>
                                            <a href="{{ route('suggestion.show',$suggestion->id) }}" class="btn btn-info">{{trans('admin.details')}}</a>

                                            <form id="delete-form-{{ $suggestion->id }}" action="{{ route('suggestion.destroy',$suggestion->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('admin.deletemsg')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $suggestion->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }">{{trans('admin.delete')}}</button>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>




@endsection