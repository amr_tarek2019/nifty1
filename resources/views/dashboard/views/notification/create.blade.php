@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('notification.createnotification')}} </h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item">{{trans('notification.notifications')}} </li>
                                <li class="breadcrumb-item active">   {{trans('notification.createnotification')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('notification.createnotification')}}</h5>
                        </div>
                        <form class="form theme-form" method="POST" action="{{ route('notification.store') }}" enctype="multipart/form-data">
                        @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('notification.title')}}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="title" parsley-trigger="change"
                                                       placeholder="Enter title" class="form-control" id="title">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('notification.text')}}</label>
                                            <div class="col-sm-9">
                                                <textarea required class="form-control" name="text" id="text"  cols="8" rows="8"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary" type="submit">{{trans('dashboard.submit')}}</button>
                                    <input class="btn btn-light" type="reset" value="Cancel">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



























{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">{{trans('notification.createnotification')}} </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="{{route('dashboard')}}">{{trans('dashboard.nift')}}</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="{{route('notification.index')}}">{{trans('notification.notifications')}} </a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                {{trans('notification.createnotification')}}--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="card-box">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-12 col-md-4">--}}
{{--                                @include('dashboard.partials.msg')--}}

{{--                                <div class="card-box">--}}
{{--                                    <h4 class="header-title m-t-0 m-b-30">{{trans('notification.createnotification')}}</h4>--}}

{{--                                    <form method="POST" action="{{ route('notification.store') }}" enctype="multipart/form-data">--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="userName">{{trans('notification.title')}}</label>--}}
{{--                                            <input type="text" name="title" parsley-trigger="change"--}}
{{--                                                   placeholder="Enter phone" class="form-control" id="title">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>{{trans('notification.text')}}</label>--}}
{{--                                            <div>--}}
{{--                                                <textarea required class="form-control" name="text" id="text"  cols="8" rows="8"></textarea>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <button type="submit" class="btn btn-purple waves-effect waves-light">{{trans('dashboard.submit')}}</button>--}}
{{--                                    </form>--}}
{{--                                </div> <!-- end card-box -->--}}

{{--                            </div> <!-- end col -->--}}



{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- End row -->--}}



{{--        </div> <!-- container -->--}}

{{--    </div> <!-- content -->--}}

@endsection