@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('notification.notificationstable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item active"> {{trans('notification.notifications')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('notification.create') }}" class="btn btn-primary">{{trans('notification.addnew')}} </a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('notification.notificationstable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> {{trans('notification.username')}}</th>
                                        <th>{{trans('notification.title')}}</th>
                                        <th>{{trans('notification.text')}}</th>
                                        <th> {{trans('notification.created')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($notifications as $key=>$notification)

                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $key + 1 }}</td>
                                            <td>{{ $notification->user->name}}</td>
                                            <td>{{ $notification->title }}</td>
                                            <td>{{ $notification->text }}</td>
                                            <td>{{ $notification->created_at }}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


























{{--    <div class="content">--}}
{{--        <div class="container">--}}


{{--            <div class="row">--}}
{{--                <div class="col-xs-12">--}}
{{--                    <div class="page-title-box">--}}
{{--                        <h4 class="page-title">{{trans('notification.notifications')}} </h4>--}}
{{--                        <ol class="breadcrumb p-0 m-0">--}}
{{--                            <li>--}}
{{--                                <a href="#">{{trans('dashboard.nift')}}</a>--}}
{{--                            </li>--}}
{{--                            <li class="active">--}}
{{--                                {{trans('notification.notifications')}}--}}
{{--                            </li>--}}
{{--                        </ol>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- end row -->--}}

{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <a href="{{ route('notification.create') }}" class="btn btn-primary">{{trans('notification.addnew')}} </a>--}}
{{--                    @include('dashboard.partials.msg')--}}
{{--                    <div class="card-box table-responsive">--}}
{{--                        <h4 class="m-t-0 header-title"><b>{{trans('notification.notificationstable')}}</b></h4>--}}
{{--                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">--}}

{{--                            <table id="datatable" class="table table-striped table-bordered">--}}
{{--                                <thead>--}}
{{--                                <tr role="row">--}}
{{--                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">--}}
{{--                                        #</th>--}}
{{--                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">--}}
{{--                                        {{trans('notification.username')}}</th>--}}
{{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 274px;">--}}
{{--                                        {{trans('notification.title')}}</th>--}}
{{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 116px;">--}}
{{--                                        {{trans('notification.text')}}</th>--}}
{{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 116px;">--}}
{{--                                        {{trans('notification.created')}}</th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                @foreach($notifications as $key=>$notification)--}}

{{--                                    <tr role="row" class="odd">--}}
{{--                                        <td class="sorting_1">{{ $key + 1 }}</td>--}}
{{--                                        <td>{{ $notification->user->name}}</td>--}}
{{--                                        <td>{{ $notification->title }}</td>--}}
{{--                                        <td>{{ $notification->text }}</td>--}}
{{--                                        <td>{{ $notification->created_at }}</td>--}}

{{--                                    </tr>--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}


{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}



{{--            </div> <!-- container -->--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection


