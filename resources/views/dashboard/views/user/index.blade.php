@extends('dashboard.layouts.master')
@section('content')







    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('user.usersTable')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">{{trans('dashboard.nift')}}</li>
                                <li class="breadcrumb-item"> {{trans('user.users')}}</li>
                                <li class="breadcrumb-item active">{{trans('user.usersTable')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('user.create') }}" class="btn btn-primary">{{trans('user.addnew')}}</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('user.usersTable')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('dashboard.username')}}</th>
                                        <th>{{trans('user.email')}}</th>
                                        <th>{{trans('user.created')}}</th>
                                        <th>{{trans('user.updated')}}</th>
                                        <th> {{trans('user.actions')}}</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)

                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $key + 1 }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->created_at }}</td>
                                            <td>{{ $user->updated_at }}</td>
                                            <td>
                                                <a href="{{ route('user.edit',$user->id) }}" class="btn btn-info">{{trans('admin.edit')}}</a>

                                                <form id="delete-form-{{ $user->id }}" action="{{ route('user.destroy',$user->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('{{trans('user.deletemsg')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">{{trans('admin.delete')}}</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->

            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>









    {{--    <div class="content">--}}
    {{--        <div class="container">--}}


    {{--            <div class="row">--}}
    {{--                <div class="col-xs-12">--}}
    {{--                    <div class="page-title-box">--}}
    {{--                        <h4 class="page-title">users </h4>--}}
    {{--                        <ol class="breadcrumb p-0 m-0">--}}
    {{--                            <li>--}}
    {{--                                <a href="#">Nift</a>--}}
    {{--                            </li>--}}
    {{--                            <li class="active">--}}
    {{--                                Users--}}
    {{--                            </li>--}}
    {{--                            <li class="active">--}}
    {{--                                users--}}
    {{--                            </li>--}}
    {{--                        </ol>--}}
    {{--                        <div class="clearfix"></div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- end row -->--}}

    {{--            <div class="row">--}}
    {{--                <div class="col-sm-12">--}}
    {{--                    <a href="{{ route('user.create') }}" class="btn btn-primary">Add New</a>--}}
    {{--                    @include('dashboard.partials.msg')--}}
    {{--                    <div class="card-box table-responsive">--}}
    {{--                        <h4 class="m-t-0 header-title"><b>users Table</b></h4>--}}
    {{--                        <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">--}}

    {{--                            <table id="datatable" class="table table-striped table-bordered">--}}
    {{--                                <thead>--}}
    {{--                                <tr role="row">--}}
    {{--                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">--}}
    {{--                                        #</th>--}}
    {{--                                    <th class="sorting_asc" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 163px;">--}}
    {{--                                        User Name</th>--}}
    {{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 274px;">--}}
    {{--                                        Email</th>--}}
    {{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 116px;">--}}
    {{--                                        Created At</th>--}}
    {{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 51px;">--}}
    {{--                                        Updated At</th>--}}
    {{--                                    <th class="sorting" tabindex="0" aria-controls="datatable-buttons" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 97px;">--}}
    {{--                                        Actions</th>--}}
    {{--                                </tr>--}}
    {{--                                </thead>--}}
    {{--                                <tbody>--}}
    {{--                                @foreach($users as $key=>$user)--}}

    {{--                                    <tr role="row" class="odd">--}}
    {{--                                        <td class="sorting_1">{{ $key + 1 }}</td>--}}
    {{--                                        <td>{{ $user->name }}</td>--}}
    {{--                                        <td>{{ $user->email }}</td>--}}
    {{--                                        <td>{{ $user->created_at }}</td>--}}
    {{--                                        <td>{{ $user->updated_at }}</td>--}}
    {{--                                        <td>--}}
    {{--                                            <a href="{{ route('user.edit',$user->id) }}" class="btn btn-info active"><i class="material-icons"><i class="mdi mdi-pencil-box"></i></i></a>--}}

    {{--                                            <form id="delete-form-{{ $user->id }}" action="{{ route('user.destroy',$user->id) }}" style="display: none;" method="POST">--}}
    {{--                                                @csrf--}}
    {{--                                            </form>--}}
    {{--                                            <button type="button" class="btn btn-danger active" onclick="if(confirm('Are you sure? You want to delete this?')){--}}
    {{--                                                    event.preventDefault();--}}
    {{--                                                    document.getElementById('delete-form-{{ $user->id }}').submit();--}}
    {{--                                                    }else {--}}
    {{--                                                    event.preventDefault();--}}
    {{--                                                    }"><i class="material-icons"><i class="mdi mdi-delete"></i></i></button>--}}
    {{--                                        </td>--}}
    {{--                                    </tr>--}}
    {{--                                @endforeach--}}
    {{--                                </tbody>--}}
    {{--                            </table>--}}


    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}



    {{--            </div> <!-- container -->--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection


