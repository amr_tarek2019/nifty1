<div class="page-sidebar">

    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{Auth::User()->image}}" alt="#">
                <div class="profile-edit"><a href="{{route('profile.index')}}" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{Auth::User()->name}}</h6>
            <p>{{Auth::User()->user_type}}.</p>
        </div>
        <ul class="sidebar-menu">
            @if(Auth::user()->user_type=='admin')
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i><span> {{trans('dashboard.dashboard')}}</span></a></li>
            @endif
                @if(Auth::user()->user_type=='admin')
            <li><a class="sidebar-header" href="{{route('admin.index')}}"><i data-feather="user-plus"></i><span> {{trans('dashboard.admins')}} </span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin')
                    <li><a class="sidebar-header" href="{{route('moderator.index')}}"><i data-feather="user-check"></i><span> {{trans('dashboard.moderators')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')
                    <li><a class="sidebar-header" href="{{route('user.index')}}"><i data-feather="users"></i><span> {{trans('dashboard.users')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin')
            <li><a class="sidebar-header" href="{{route('station.index')}}"><i data-feather="users"></i><span> {{trans('dashboard.stations')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li><a class="sidebar-header" href="{{route('charity.index')}}"><i data-feather="home"></i><span> {{trans('dashboard.charities')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li><a class="sidebar-header" href="{{route('payments.index')}}"><i data-feather="users"></i><span> {{trans('dashboard.payments')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin'||Auth::user()->user_type=='moderator')

                <li><a class="sidebar-header" href="{{route('slider.index')}}"><i data-feather="sliders"></i><span>  {{trans('dashboard.sliders')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin')
                <li><a class="sidebar-header" href="{{route('privacy.index')}}"><i data-feather="paperclip"></i><span> {{trans('dashboard.privacy')}} </span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin')

                <li><a class="sidebar-header" href="{{route('suggestion.index')}}"><i data-feather="mail"></i><span> {{trans('dashboard.suggestions')}} </span></a></li>
                @endif

                @if(Auth::user()->user_type=='admin')
                <li><a class="sidebar-header" href="{{route('notification.index')}}"><i data-feather="bell"></i><span>{{trans('dashboard.pushNotification')}}</span></a></li>
                @endif
                @if(Auth::user()->user_type=='admin')

                <li><a class="sidebar-header" href="{{route('settings.index')}}"><i data-feather="settings"></i><span> {{trans('dashboard.settings')}}</span></a></li>
                @endif


        </ul>
    </div>
</div>