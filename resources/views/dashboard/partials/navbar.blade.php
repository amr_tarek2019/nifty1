<div class="page-main-header">
    <div class="main-header-right row">
        <div class="main-header-left d-lg-none">
            <div class="logo-wrapper"><a href="index.html"><img src="{{ asset('assets/dashboard/images/fav_icon.png') }}" alt=""></a></div>
        </div>
        <div class="mobile-sidebar d-block">
            <div class="media-body text-right switch-sm">
                <label class="switch"><a href="#"><i id="sidebar-toggle" data-feather="align-left"></i></a></label>
            </div>
        </div>
        <div class="nav-right col p-0">
            <ul class="nav-menus">
                <li>

                </li>
                <li></li>
                <li class="onhover-dropdown"><a class="txt-dark" href="index.html#">
                        <h6>
                            @if(request()->segment(1)=='en')
                            EN
                        @else
                        العربية
                        @endif</h6></a>
                    @if(request()->segment(1)=='en')
                    <ul class="language-dropdown onhover-show-div p-20">
                        @else
                            <ul class="language-dropdown onhover-show-div p-20" style="direction: rtl;text-align: right;float: right">
                                @endif
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li><a  rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" ><i class="flag-icon flag-icon"></i>  {{ $properties['native'] }}</a></li>
                        @endforeach

                    </ul>
                </li>
                <li class="onhover-dropdown"><i data-feather="bell"></i><span class="dot"></span>
                    @if(request()->segment(1)=='en')
                    <ul class="notification-dropdown onhover-show-div">
                        @else
                            <ul class="notification-dropdown onhover-show-div" style="direction: rtl;text-align: right;float: right">
                                @endif
                        <li>{{trans('dashboard.messages')}} <span class="badge badge-pill badge-primary pull-right">{{countUnreadMsg()}}</span></li>
                        @if(countUnreadMsg()>0)
                            @foreach(unreadMsg() as $keyMessages => $valueMessage)
                        <li>
                            <a href="{{route('suggestion.show',$valueMessage->id)}}">
                            <div class="media">
                                <div class="media-body">
                                    <h6 class="mt-0"><span><i class="shopping-color" data-feather="mail"></i></span>{{$valueMessage->user->name}}<small class="pull-right">{{$valueMessage->created_at}}</small></h6>
                                    <p class="mb-0">{{str_limit($valueMessage->suggestion,10)}}.</p>
                                </div>
                            </div>
                            </a>
                        </li>
                            @endforeach

                        <li class="bg-light txt-dark"><a href="{{route('suggestion.index')}}"></a> {{trans('dashboard.seeAllMessages')}}</li>
                                @else
                                                        <li class="all-msgs text-center">
                                                            <p class="m-0"><a href="#">{{trans('dashboard.nomessagesfound')}}</a></p>
                                                        </li>
                        @endif
                    </ul>
                </li>
                <li class="onhover-dropdown">
                    <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="{{Auth::User()->image}}" alt="header-user">
                        <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                    </div>
                    @if(request()->segment(1)=='en')
                    <ul class="profile-dropdown onhover-show-div p-20">
                        @else
                            <ul class="profile-dropdown onhover-show-div p-20" style="direction: rtl;text-align: right;float: right;margin-right: -40px;">
                                @endif
                                @if(request()->segment(1)=='en') <li>
                                    @else
                                    <li style="text-align: right">
                                        @endif
                                        <a href="{{route('profile.index')}}"><i data-feather="user"></i>                                    {{trans('dashboard.profile')}}</a></li>
                        <li><a href="{{route('settings.index')}}"><i data-feather="settings"></i>                                    {{trans('dashboard.settings')}}</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i data-feather="log-out"></i>                                    {{trans('dashboard.logout')}}</a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</div>