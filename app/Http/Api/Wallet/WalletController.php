<?php

namespace App\Http\Controllers\Api\Wallet;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Suggestion;
use App\User;
use App\UserWallet;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WalletController extends BaseController
{
    public function index(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $data['current_balance'] = $user['balance'];
        $data['payment_method'] = $user['payment_method'];
        $response=[
            'message'=>'get data of user wallet successfully',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function update(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->balance = $request->balance;
        $user->payment_method=$request->payment_method;
        if($user->save())
        {
            $response=[
                'message'=>'user wallet data changed successfully',
                'status'=>200,
            ];
        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'this acount submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function createWallet(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();

        $validator = Validator::make($request->all(), [
            'card_number' => 'required',
            'expire_date' => 'required',
            'cvv' => 'required',
            'cardholder_name' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $otp_code = rand(1111, 9999);
        $wallet=new UserWallet();
        $wallet->user_id=$user->id;
        $wallet->card_number = $request->card_number;
        $wallet->expire_date = $request->expire_date;
        $wallet->cvv = $request->cvv;
        $wallet->cardholder_name = $request->cardholder_name;
        $wallet->otp_code = $otp_code;
        $wallet->save();
        $response=[
            'message'=>'Wait for otp to be sent to your phone for verification',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }
    public function verifyOtpCode(Request $request)
    {
        $this->validate($request,[
            'otp_code' => 'required',
        ]);
        $user = UserWallet::where('otp_code', request('otp_code'))->first();
        if (!empty($user)) {
            $user->otp_code='0';
            $user->save();
            $response=[
                'message'=>'your payment verified',
                'status'=>200,
            ];

            return \Response::json($response,200);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>'otp code not found',
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    public function addToBalance(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $last_balance = User::where('id',$user)->first();
        $validator = Validator::make($request->all(), [
            'balance' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $balance = $request->balance;

        $total=$last_balance['balance']+$balance;
        $last_balance->balance = $total;
        if($last_balance->save()){
            $response=[
                'message'=>'balance changed successfully',
                'status'=>200,
                'data'=>$total
            ];
            return \Response::json($response,200);
            return $total;
        }

        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }
}
