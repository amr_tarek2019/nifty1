<?php

namespace App\Http\Controllers\Api\Stations;

use App\Category;
use App\Charity;
use App\City;
use App\CompanySeller;
use App\Privacy;
use App\Reservation;
use App\Slider;
use App\StationImages;
use App\TakeDownContainer;
use App\User;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Station;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        // $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        // $user = User::where('jwt_token',$jwt)->first();
        // $user->longitude = $request->longitude;
        // $user->latitude=$request->latitude;

        $sliders = Slider::select('id','title_'.$lang. ' as title'
            ,'text_'.$lang. ' as text','image')->get();

        // $charities = Charity::select('id','name_'.$lang. ' as name','image')->get();

        // $stationsRate=Station::select('id','name_'.$lang. ' as name'
        //     ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
        //     'icon','image1','image2')->orderBy('rate', 'desc')->take(2)->get();
        $stationsRate=Station::select("id",'name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
            'icon','image1','image2')->orderBy('rate', 'desc')->take(2)->get();



        $station_list=Station::select("id",'name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
            'icon','image1','image2')->take(2)->get();

        $data['sliders'] = $sliders;
        // $data['charities'] = $charities;
        $data['rated_stations'] = $stationsRate;
        $data['location_status']=$station_list;

        if($data)
        {
            $response=[
                'message'=>'Home',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'Home',
                'status'=>404,
                'data'=>trans('api.somethingwentwrong'),
            ];
        }

        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRate(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $station_list=Station::select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
            'icon','image1','image2')->orderBy('rate', 'desc')->take(2)->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>200,
            'data'=>$station_list,
        ];

        return \Response::json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function indexLocation(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        $station_list=DB::table("stations")
            ->select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->take(2)->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>200,
            'data'=>$station_list,
        ];

        return \Response::json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SearchByRate(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $validator = Validator::make($request->all(), [
            'rate' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $rate = $request->rate;
        $stations = Station::select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region',
            'rate','image','description_'.$lang. ' as description',
            'icon','image1','image2')->where([
            ['rate', 'LIKE', '%' . $rate . '%']])->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>200,
            'data'=>$stations,
        ];
        return \Response::json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SearchByLocation(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        $station_list=DB::table("stations")
            ->select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->get();
        $response=[
            'message'=>'get data of stations successfully',
            'status'=>200,
            'data'=>$station_list,
        ];

        return \Response::json($response,200);
    }


    //search with word
    public function SearchByWord(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        if($lang=='en')
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $name_en = $request->name;
            $stations = Station::select('id','name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2')->where([
                ['name_en', 'LIKE', '%' . $name_en . '%']])->get();
            $response=[
                'message'=>'get data of stations successfully',
                'status'=>200,
                'data'=>$stations,
            ];
            return \Response::json($response,200);
        }
        else{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $name_ar = $request->name;
            $stations = Station::select('id','name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2')->where([
                ['name_ar', 'LIKE', '%' . $name_ar . '%']])->get();
            $response=[
                'message'=>'get data of stations successfully',
                'status'=>200,
                'data'=>$stations,
            ];
            return \Response::json($response,200);
        }
    }




    //search with rate and location
    public function SearchByRateAndLocation(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'longitude' => '',
            'latitude' => '',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;
        $station_list=DB::table("stations")
            ->select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->get();

        //   $validator = Validator::make($request->all(), [
        //     'rate_from' => 'required',
        //     'rate_to' => 'required',
        // ]);
        // if ($validator->fails()) {
        //     return $this->sendError('Validation Error.', $validator->errors());
        // }
        $stations = Station::whereBetween('rate',[$request->rate_from,$request->rate_to])->select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
            'icon','image1','image2')->get();
        $data['search_locations'] = $station_list;
        $data['search_rates'] = $stations;
        // return $stations;
        $response=[
            'message'=>'search results',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);
    }




//    public function SearchByRegionE(Request $request)
//    {
//        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
//        $validator = Validator::make($request->all(), [
//            'region_e' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return $this->sendError('Validation Error.', $validator->errors());
//        }
//        $region_e = $request->region_e;
//        $stations = Station::select('id','name_'.$lang. ' as name'
//            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
//            'icon','image1','image2')->where([
//            ['region_e', 'LIKE', '%' . $region_e . '%']])->get();
//        $response=[
//            'message'=>'get data of stations successfully',
//            'status'=>200,
//            'data'=>$stations,
//        ];
//        return \Response::json($response,200);
//    }
//
//    public function SearchByRegionA(Request $request)
//    {
//        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
//        $validator = Validator::make($request->all(), [
//            'region_a' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return $this->sendError('Validation Error.', $validator->errors());
//        }
//        $region_a = $request->region_a;
//        $stations = Station::select('id','name_'.$lang. ' as name'
//            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','qr','description_'.$lang. ' as description',
//            'icon','image1','image2')->where([
//            ['region_a', 'LIKE', '%' . $region_a . '%']])->get();
//        $response=[
//            'message'=>'get data of stations successfully',
//            'status'=>200,
//            'data'=>$stations,
//        ];
//        return \Response::json($response,200);
//    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show(Request $request)
    {
//        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'e';
//        $this->validate($request,[
//            'id' => 'required',
//        ]);
//        $station = Station::where('id', request('id'))->first();
//        if (!empty($station)) {
//            $response=[
//                'message'=>'get data of station successfully',
//                'status'=>200,
//                'data'=>$station,
//            ];
//
//            return \Response::json($response,200);
//
//        }else{
//            // return \Response::json('code activation not found',404);
//            $response=[
//                'message'=>'your qr code not found',
//                'status'=>404,
//            ];
//
//            return \Response::json($response,200);
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;

        $stations=Station::where('id',$request->id)->select("id",'name_'.$lang. ' as name'
            ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
            'icon','image1','image2'
            ,DB::raw("3959 * round(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance",2))->first();

        // $station_Items = [];
        // $station_list =[];
        // foreach($stations as $station)
        // {
        //     $station_Items['name_station'] = \App\Station::where('id',$station->id)->select('name_'. $lang . ' as name')->first();
        //     $station_Items['region_station'] = \App\Station::where('id',$station->id)->select('region_'. $lang . ' as region')->first();
        //     $station_Items['description_station'] = \App\Station::where('id',$station->id)->select('description_'. $lang . ' as description')->first();
        //     $station_Items['image'] = $station['image'];
        //     $station_Items['image1'] = $station['image'];
        //     $station_Items['image2'] = $station['image2'];
        //     $station_Items['icon'] = $station['icon'];
        //     $station_Items['qr_code'] = $station['qr_code'];
        //     $station_Items['rate'] = $station['rate'];
        //     $station_Items['latitude'] = $station['latitude'];
        //     $station_Items['latitude'] = $station['latitude'];
        //     $station_Items['longitude'] = $station['longitude'];

        //     $station_list[] = $station_Items;
        //     return $station_list;

        // }

        if($stations){
            $response=[
                'message'=>'get data of station successfully',
                'status'=>200,
                'data'=>$stations,
            ];
        }else{
            $response=[
                'message'=>'data not found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
    }

}
