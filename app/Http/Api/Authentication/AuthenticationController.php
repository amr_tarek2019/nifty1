<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthenticationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'password' => 'required',
            'confirm_password'=>'required',
            'payment_method' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>301,
                'message'=>trans('api.submitted'),
            ];
        }
        if ($request->password !=null&&($request->password==$request->confirm_password)){
            $user = User::create(array_merge($request->all(),[
                'jwt_token' => $jwt_token,
                'verify_code' => $verify_code,
                'balance'=>'0',
                'user_type'=>'user'
            ]));
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['balance'] = $user['balance'];
            $response=[
                'status'=>200,
                'data'=>$data,
                'message'=>'User register successfully.',
            ];
            return \Response::json($response,200);
        }else{
            $response=[
                'status'=>400,
                'message'=>'password is not matching with confirm password.',
            ];
            return \Response::json($response,200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firebase_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (auth()->attempt(['email' => $request->input('email'),
            'password' => $request->input('password')])) {
            $user = auth()->user();
            $verify_code = rand(1111, 9999);
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['balance'] = $user['balance'];
            $data['firebase_token'] = $user['firebase_token'];
            if($user->user_status=='0')
            {
                if($user){
                    $user->verify_code=$verify_code;
                    $user->save();
                }
                return response()->json([
                    'status'=>303,
                    'message'=>trans('api.notactive'),
                    'verify_code' => $verify_code,
                ]);
            }
            if($user) {
                $user->firebase_token = $request->firebase_token;
                $user->save();
                return response()->json([
                    'status'=>200,
                    'message'=>trans('api.logged'),
                    'data'=>$data
                ]);
            }
        } else {
            return response()->json([
                'status'=>404,
                'message'=>trans('api.check'),
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required'
        ]);
        $user = User::where('phone', request('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>trans('api.code'),
                'status'=>200,
            ];
            return \Response::json($response,200);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>'phone not found',
                'status'=>402,
            ];
            return \Response::json($response,402);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function verifyCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required',
            'firebase_token'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code=null;
            $user->user_status=1;
            $user->firebase_token=$request->firebase_token;
            $user->save();
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['balance'] = $user['balance'];
            $data['firebase_token'] = $user['firebase_token'];
            $response=[
                'message'=>trans('api.verify'),
                'status'=>200,
                'data'=>$data,

            ];

            return \Response::json($response,200);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>trans('api.codenotfound'),
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
//        $user=User::find($request->id);
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token', $jwt)->first();
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null&&($request->password==$request->password_confirmation)){
            $user->password=$request->password_confirmation;
        }
        if ($user->save())
        {
            $response=[
                'message'=>trans('api.passwordchangedsuccess'),
                'status'=>200,
                'data'=>$user,
            ];

            return \Response::json($response,200);

        }else{
            $response=[
                'message'=>trans('api.somethingwentwrong'),
                'status'=>400,
                'data'=>$user,
            ];

            return \Response::json($response,401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app)
    {
        //
    }
}

