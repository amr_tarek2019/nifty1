<?php

namespace App\Http\Controllers\Api\Reviews;

use App\CompanySeller;
use App\Review;
use App\Station;
use App\Suggestion;
use App\User;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $getAverageRatingOfStationReviews=Review::where('station_id',$request->station_id)->select('rating')->avg('rating');
        $getCountRatingOfStationReviews=Review::where('station_id',$request->station_id)->select('rating')->count('rating');
        $countNumberOfFive=Review::select('rating')->where('rating','5')->where('station_id',$request->station_id)->count();
        $countNumberOfFour=Review::select('rating')->where('rating','4')->where('station_id',$request->station_id)->count();
        $countNumberOfThree=Review::select('rating')->where('rating','3')->where('station_id',$request->station_id)->count();
        $countNumberOfTwo=Review::select('rating')->where('rating','2')->where('station_id',$request->station_id)->count();
        $countNumberOfOne=Review::select('rating')->where('rating','1')->where('station_id',$request->station_id)->count();

        $data['rate'] = round($getAverageRatingOfStationReviews,2);
        $data['count'] = $getCountRatingOfStationReviews;
        $data['countNumberOfReviewsOfFive']=$countNumberOfFive;
        $data['countNumberOfFour']=$countNumberOfFour;
        $data['countNumberOfThree']=$countNumberOfThree;
        $data['countNumberOfTwo']=$countNumberOfTwo;
        $data['countNumberOfOne']=$countNumberOfOne;
        $response=[
            'message'=>'reviews',
            'status'=>200,
            'data'=>$data,
        ];

        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $station=Station::where('id',$request->station_id)->select('id')->first();
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $review=new Review();
        $review->user_id=$user->id;
        $review->station_id=$station->id;
        $review->rating = $request->rating;
        $review->comment = $request->comment;
        $review->save();
        $response=[
            'message'=>'Review sent successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $reviews=Review::where('station_id',$request->station_id)->get();
        $review_Items = [];
        $review_list =[];
        foreach($reviews as $review)
        {
            $review_Items['id'] = $review->id;
            $review_Items['rating'] = $review->rating;
            $review_Items['comment'] = $review->comment;
            $review_Items['station_details'] = Station::where('id',$request->station_id)->select('name_'.$lang.' as name')->first();
            $review_Items['user_details'] = User::where('id',$review->user_id)->select('id','name','image')->first();
            $review_Items['created_at'] = $review->created_at;
            $review_list[] = $review_Items;
        }
        $response=[
            'message'=>'get data of station reviews successfully',
            'status'=>200,
            'data'=>$review_list,
        ];
        return \Response::json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAverageRatingOfStationReviews(Request $request)
    {
        $reviews=Review::select(DB::raw('avg(rating) As Ratings'))->where('station_id',$request->station_id)->get();
        $response=[
            'message'=>'get data of average rating of station reviews successfully',
            'status'=>200,
            'data'=>$reviews,
        ];
        return \Response::json($response,200);
    }

    public function getCountRatingOfStationReviews(Request $request)
    {
        $reviews=Review::select(DB::raw('count(id) As Count' ))->where('station_id',$request->station_id)->get();
        $response=[
            'message'=>'get data count ratings of station reviews successfully',
            'status'=>200,
            'data'=>$reviews,
        ];
        return \Response::json($response,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
