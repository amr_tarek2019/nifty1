<?php

namespace App\Http\Controllers\Dashboard;

use App\Charity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CharitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charities=Charity::all();
        return view('dashboard.views.charity.index',compact('charities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.charity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_e' => 'required',
            'name_a' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $charity = new Charity();
        $charity->name_e = $request->name_e;
        $charity->name_a = $request->name_a;
        $charity->image =  $request->image;

        if ($charity->save())
        {
            return redirect()->route('charity.index')->with('successMsg','Charity Created Successfully');
        }
        return redirect()->route('charity.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $charity = Charity::find($id);
        return view('dashboard.views.charity.edit',compact('charity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $charity = Charity::find($id);
        $charity->name_e = $request->name_e;
        $charity->name_a = $request->name_a;
        $charity->image =  $request->image;
        if ($charity->save())
        {
            return redirect()->route('charity.index')->with('successMsg','Charity Updated Successfully');
        }
        return redirect()->route('charity.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $charity = Charity::find($id);
        $charity->delete();
        return redirect()->route('charity.index')->with('successMsg','Charity Deleted Successfully');
    }
}
