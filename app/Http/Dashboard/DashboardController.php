<?php

namespace App\Http\Controllers\Dashboard;

use App\Suggestion;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $numbers_of_users = DB::SELECT("select id, count(*) as count, date(created_at) as date from users WHERE user_type = 'user' and date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        $numbers_of_stations = DB::SELECT("select id, count(*) as count, date(created_at) as date from stations WHERE date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        $numbers_of_charities = DB::SELECT("select id, count(*) as count, date(created_at) as date from charity WHERE date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");

        return view('dashboard.views.dashboard.index',compact('numbers_of_users','numbers_of_stations',
            'numbers_of_charities'));
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect()->route('login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function suggestionsCount(){
        $suggestionsCount= DB::table('suggestions')
            ->orderBy('id', 'desc')
            ->where('view', 0)
            ->select('id')
            ->distinct()
            ->count();
        return response()->json($suggestionsCount);
    }

    public function requestsCount(){
        $requestsCount= DB::table('users')
            ->orderBy('id', 'desc')
            ->where('user_status', '0')
            ->where('user_type', 'user')
            ->select('id')
            ->distinct()
            ->count();
        return response()->json($requestsCount);
    }

    public function getSuggestionsNotifications()
    {
        $suggetions=Suggestion::where('view',0)->get();
        return view('dashboard.views.dashboard.index',compact('suggetions'));
    }


}
