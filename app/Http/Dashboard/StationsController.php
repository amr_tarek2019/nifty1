<?php

namespace App\Http\Controllers\Dashboard;

use App\Privacy;
use App\Station;
use App\UserStation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use niklasravnsborg\LaravelPdf\Pdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class StationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations=Station::all();
        return view('dashboard.views.station.index',compact('stations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.station.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name_en' => 'required',
            'name_ar' => 'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'region_en' => 'required',
            'region_ar' => 'required',
            'rate' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'description_en'=>'required',
            'description_ar'=>'required',
            'icon'=> 'required|mimes:jpeg,jpg,bmp,png',
            'qr_code'=>'required',
            'image1' => 'required|mimes:jpeg,jpg,bmp,png',
            'image2' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $station = new Station();
        $station->user_id=Auth::user()->id;
        $station->name_en = $request->name_en;
        $station->name_ar = $request->name_ar;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->region_en = $request->region_en;
        $station->region_ar = $request->region_ar;
        $station->rate=$request->rate;
        $station->image =  $request->image;
        $station->description_en = $request->description_en;
        $station->description_ar = $request->description_ar;
        $station->icon =  $request->icon;
        $station->qr_code =  $request->qr_code;
        $station->image1 =  $request->image1;
        $station->image2 =  $request->image2;
        QrCode::format('png')->size(500)->generate( $station->qr_code, public_path('uploads/station/qr_code/' .  $station->qr_code . '.png'));

        //dd($station);
        if ($station->save())
        {
            return redirect()->route('station.index')->with('successMsg','Station Created Successfully');
        }
        return redirect()->route('station.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $station = Station::find($id);
        return view('dashboard.views.station.edit',compact('station'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $station = Station::find($id);
        $station->user_id=Auth::user()->id;
        $station->name_en = $request->name_en;
        $station->name_ar = $request->name_ar;
        $station->latitude = $request->latitude;
        $station->longitude = $request->longitude;
        $station->region_en = $request->region_en;
        $station->region_ar = $request->region_ar;
        $station->rate=$request->rate;
        $station->image =  $request->image;
        $station->description_e = $request->description_en;
        $station->description_a = $request->description_ar;
        $station->icon =  $request->icon;
        $station->qr_code =  $request->qr_code;
        $station->image1 =  $request->image1;
        $station->image2 =  $request->image2;
        QrCode::format('png')->size(500)->generate( $station->qr_code, public_path('uploads/station/qr_code/' .  $station->qr_code . '.png'));

        if ($station->save())
        {
            return redirect()->route('station.index')->with('successMsg','Station Updated Successfully');
        }
        return redirect()->route('station.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $station = Station::find($id);
        $station->delete();
        return redirect()->route('station.index')->with('successMsg','Station Deleted Successfully');
    }

    function printQr($id) {
        $station = Station::find($id);
        $pdf = \niklasravnsborg\LaravelPdf\Facades\Pdf::loadView('dashboard.views.station.pdf', compact('station'));
        return $pdf->download('qr.pdf');
    }
}
