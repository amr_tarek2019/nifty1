<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['phone', 'email' , 'website' , 'facebook','whatsapp','instagram','about_e','about_a'];
    protected $table='settings';

}
