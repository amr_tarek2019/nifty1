<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    protected $table='user_wallet';
    protected $fillable=['user_id', 'card_number', 'expire_date', 'cvv', 'cardholder_name', 'otp_code'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
