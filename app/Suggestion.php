<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table='suggestions';
    protected $fillable=[ 'user_id','email', 'suggestion','view'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
