<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_e');
            $table->string('name_a');
            $table->decimal('latitude',8,6	);
            $table->decimal('longitude',9,6);
            $table->string('region_e');
            $table->string('region_a');
            $table->integer('rate');
            $table->string('image');
            $table->string('image1');
            $table->string('image2');
            $table->string('qr_code');
            $table->string('icon');
            $table->text('description_e');
            $table->text('description_a');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
